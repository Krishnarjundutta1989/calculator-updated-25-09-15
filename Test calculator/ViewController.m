//
//  ViewController.m
//  Test calculator
//
//  Created by Anil Kumar on 9/25/15.
//  Copyright (c) 2015 Anil Kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    UITextField *txtInput;
    BOOL isAnyOperatorClicked;
    float firstValue;
    int secondValue;
    NSMutableString *numberClicked;
    NSMutableArray *numberStoreInArray;
    NSMutableArray * operatorStoreInArray;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViewLayout];
    numberStoreInArray = [NSMutableArray new];
    operatorStoreInArray = [NSMutableArray new];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)setupViewLayout {
    //
    //    isAnyOperatorClicked = NO;
    //    isMultiValue = NO;
    
    UIView *view1 =[UIView new];
    view1.frame = CGRectMake(0, 0, 375, 667);
    view1. backgroundColor = [UIColor blackColor];
    [self.view addSubview:view1];
    
    //    firstValue = -1;
    //    actionType = -1;
    //    oldValue = -1;
    //    resultValue = -1;
    //
    //    currentNumber = 0;
    
    
    
    /*
     llbResult = [UILabel new];
     llbResult.frame = CGRectMake(2, 1, [[UIScreen mainScreen] bounds].size.width - 4, 167);
     
     //iPhone 4
     if ([[UIScreen mainScreen] bounds].size.height == 480) {
     }
     //iPhone 5
     else if ([[UIScreen mainScreen] bounds].size.height == 568) {
     
     }
     
     llbResult.backgroundColor = [UIColor colorWithRed:140/255.0 green:142/255.0 blue:144/255.0 alpha:1];
     llbResult.text = @"Result";
     llbResult.font = [UIFont fontWithName:@"Georgia" size:60.0];
     llbResult.textColor = [UIColor whiteColor];
     llbResult.textAlignment = NSTextAlignmentRight;
     [self.view addSubview:llbResult];
     
     */
    
    txtInput = [UITextField new];
    txtInput.frame = CGRectMake(2, 1, [[UIScreen mainScreen] bounds].size.width - 4, 167);
    //iPhone 4
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        
    }
    //iPhone 5
    else if ([[UIScreen mainScreen] bounds].size.height == 568) {
        
    }
    txtInput.backgroundColor = [UIColor grayColor];
    txtInput.textAlignment = NSTextAlignmentRight;
    txtInput.font = [UIFont fontWithName:@"Georgia" size:60.0];
    txtInput. textColor = [UIColor whiteColor];
    [self.view addSubview:txtInput];
    
    
    UIButton*btn1 = [UIButton new];
    btn1.frame = CGRectMake(2, 445,91,91);
    btn1.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn1 setTitle:@"1" forState:UIControlStateNormal];
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn1.tag = 1;
    [btn1 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn1];
    
    
    
    UIButton*btn2 = [UIButton new];
    btn2.frame = CGRectMake(94, 445,91,91);
    btn2.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn2 setTitle:@"2" forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn2.tag = 2;
    [btn2 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn2];
    
    
    
    UIButton*btn3 = [UIButton new];
    btn3.frame = CGRectMake(186, 445,91,91);
    btn3.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn3 setTitle:@"3" forState:UIControlStateNormal];
    [btn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn3.tag = 3;
    [btn3 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn3];
    
    
    
    UIButton*btn4 = [UIButton new];
    btn4.frame = CGRectMake(2, 353, 91, 91);
    btn4.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn4 setTitle:@"4" forState:UIControlStateNormal];
    [btn4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn4.tag = 4;
    [btn4 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn4];
    
    
    
    UIButton*btn5 = [UIButton new];
    btn5.frame = CGRectMake(94, 353, 91, 91);
    btn5.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn5 setTitle:@"5" forState:UIControlStateNormal];
    [btn5 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn5.tag = 5;
    [btn5 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn5];
    
    
    
    UIButton*btn6 = [UIButton new];
    btn6.frame = CGRectMake(186, 353, 91, 91);
    btn6.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn6 setTitle:@"6" forState:UIControlStateNormal];
    [btn6 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn6.tag = 6;
    [btn6 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn6];
    
    
    
    UIButton*btn7 = [UIButton new];
    btn7.frame = CGRectMake(2, 261, 91, 91);
    btn7.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn7 setTitle:@"7" forState:UIControlStateNormal];
    [btn7 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn7.tag = 7;
    [btn7 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn7];
    
    
    
    UIButton*btn8 = [UIButton new];
    btn8.frame = CGRectMake(94, 261, 91, 91);
    btn8.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn8 setTitle:@"8" forState:UIControlStateNormal];
    [btn8 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn8.tag = 8;
    [btn8 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn8];
    
    
    
    UIButton*btn9 = [UIButton new];
    btn9.frame = CGRectMake(186, 261, 91, 91);
    btn9.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn9 setTitle:@"9" forState:UIControlStateNormal];
    [btn9 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn9.tag = 9;
    [btn9 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn9];
    
    
    UIButton*btn0 = [UIButton new];
    btn0.frame = CGRectMake(2, 537, 183, 91);
    btn0.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn0 setTitle:@"0" forState:UIControlStateNormal];
    [btn0 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn0.tag = 0;
    [btn0 addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn0];
    
    
    UIButton*btnPoint = [UIButton new];
    btnPoint.frame = CGRectMake(186, 537, 91, 91);
    btnPoint.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btnPoint setTitle:@"." forState:UIControlStateNormal];
    [btnPoint setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnPoint.tag = 11;
    [btnPoint addTarget:self action:@selector(numberClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnPoint];
    
    
    UIButton*btnClear = [UIButton new];
    /*
     
     int width =  [[UIScreen mainScreen] bounds].size.width - 270;
     int height = [[UIScreen mainScreen] bounds].size.height -390;
     
     */
    btnClear.frame = CGRectMake(2, 169, 91,91);
    btnClear.backgroundColor =
    [UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1];
    [btnClear setTitle:@"AC" forState:UIControlStateNormal];
    [btnClear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnClear.tag = 12;
    [btnClear addTarget:self action:@selector(clearAll: ) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnClear];
    
    
    UIButton*btnPlusMinus = [UIButton new];
    btnPlusMinus.frame = CGRectMake(94, 169, 91,91);
    btnPlusMinus.backgroundColor = [UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1];
    [btnPlusMinus setTitle:@"+/-" forState:UIControlStateNormal];
    [btnPlusMinus setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [btnPlusMinus setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnPlusMinus.tag = 13;
    [btnPlusMinus addTarget:self action:@selector(PlusMinus) forControlEvents:UIControlEventTouchUpInside];
    [btnPlusMinus setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.view addSubview:btnPlusMinus];
    
    
    
    UIButton*btnPercentage = [UIButton new];
    btnPercentage.frame = CGRectMake(186, 169, 91, 91);
    btnPercentage.backgroundColor = [UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1];
    [btnPercentage setTitle:@"%" forState:UIControlStateNormal];
    [btnPercentage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnPercentage.tag = 14;
    [btnPercentage addTarget:self action:@selector(Percentage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnPercentage];
    
    
    UIButton *btnMulti = [UIButton new];
    btnMulti.frame = CGRectMake(278, 261, 95, 91);
    btnMulti.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnMulti setTitle:@"x" forState:UIControlStateNormal];
    [btnMulti setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnMulti.tag = 15;
    // currentOperation = 3;
    [btnMulti addTarget:self action:@selector(operatorClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMulti];
    
    
    UIButton*btnDiv = [UIButton new];
    btnDiv.frame = CGRectMake(278, 169, 95, 91);
    btnDiv.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnDiv setTitle:@"/" forState:UIControlStateNormal];
    [btnDiv setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnDiv.tag = 16;
    // currentOperation = 4;
    [btnDiv addTarget:self action:@selector(operatorClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDiv];
    
    
    UIButton*btnAdd = [UIButton new];
    btnAdd.frame = CGRectMake(278,445,95,91);
    btnAdd.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnAdd setTitle:@"+" forState:UIControlStateNormal];
    [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnAdd.tag = 17;
    //currentOperation = 1;
    [btnAdd addTarget:self action:@selector(operatorClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnAdd];
    
    
    
    UIButton*btnSub = [UIButton new];
    btnSub.frame = CGRectMake(278, 353, 95, 91);
    btnSub.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnSub setTitle:@"-" forState:UIControlStateNormal];
    [btnSub setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSub.tag = 18;
    //currentOperation = 2;
    [btnSub addTarget:self action:@selector(operatorClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSub];
    
    
    UIButton*btnequal = [UIButton new];
    btnequal.frame = CGRectMake(278, 537, 95, 91);
    btnequal.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnequal setTitle:@"=" forState:UIControlStateNormal];
    [btnequal setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnequal.tag = 19;
    [btnequal addTarget:self action:@selector(performEqual:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnequal];
}

- (void)numberClick: (UIButton *) sender {
    if ([txtInput.text length] == 0){
        firstValue =-1;
        numberClicked = [NSMutableString stringWithFormat:@"%ld",(long)[sender tag]];
        txtInput.text = [NSString stringWithFormat:@"%@",numberClicked];
            }
    else{
        
        if ([sender tag] == 11) {
            
            [numberClicked appendString:@"."];

        }
        else {
            [numberClicked appendString:[NSMutableString stringWithFormat:@"%ld",(long)[sender tag]]];
        }
        txtInput.text = [NSString stringWithFormat:@"%@",numberClicked];

       
    }
}
- (void) operatorClick: (UIButton *) sender {
    
    isAnyOperatorClicked = YES;
    
    //Operator store in number
    
    [numberStoreInArray addObject:numberClicked];
    
    NSLog(@"%@", [numberStoreInArray objectAtIndex: 0]);
    NSLog (@"Number of elements in array = %lu", [numberStoreInArray count]);
    txtInput.text = @"";
    
    
    //Operator store in array
    NSMutableString *operator = [NSMutableString stringWithFormat:@"%ld",(long)[sender tag]];
    [operatorStoreInArray addObject:operator];
    NSLog(@"%@", [operatorStoreInArray objectAtIndex:0]);
    NSLog (@"Number of elements in array = %lu", [operatorStoreInArray count]);
    //chk operatorStoreInArray index count > 1
    if ([operatorStoreInArray count] >1) {
        
        float first_Value = [[numberStoreInArray objectAtIndex: 0] floatValue];
        float seconed_Value = [[numberStoreInArray objectAtIndex:1] floatValue];
        NSInteger operatorNew = [[operatorStoreInArray objectAtIndex:0] integerValue];
        first_Value = [self performOperation:operatorNew first_Value:first_Value sec_Value:seconed_Value];
        txtInput.text = [NSString stringWithFormat:@"%.1f",first_Value];
        [numberStoreInArray removeAllObjects];
        [numberStoreInArray addObject:[NSString stringWithFormat:@"%.1f",first_Value]];
        [operatorStoreInArray removeObjectAtIndex: 0];
        txtInput.text = @"";

    }
    
}

-(void) performEqual: (UIButton *) sender{
    
    float numberNew = [txtInput.text floatValue] ;
    if ([operatorStoreInArray count] == 1) {
        float first_Value = [[numberStoreInArray objectAtIndex: 0] floatValue];
        NSInteger operatorNew = [[operatorStoreInArray objectAtIndex:0] floatValue];
        first_Value = [self performOperation:operatorNew first_Value:first_Value sec_Value:numberNew];
        txtInput.text = [NSString stringWithFormat:@"%.1f",first_Value];
        [numberStoreInArray removeAllObjects];
        [numberStoreInArray addObject:[NSString stringWithFormat:@"%.1f",first_Value]];
        [operatorStoreInArray removeObjectAtIndex: 0];
        
    }
}

- (float)performOperation:(NSInteger)operation first_Value:(float)first_Value sec_Value:(float)sec_Value {
    
    switch (operation) {
            
            //Multiplication
        case 15:
            return first_Value*sec_Value;
            break;
        case 16:
            return first_Value/sec_Value;
            break;
        case 17:
            return first_Value + sec_Value;
            break;
        case 18:
            return first_Value - sec_Value;
            break;
            
        default:
            break;
    }
    
    return 0;
}

- (void) clearAll:(UIButton *) sender{
    numberClicked = [NSMutableString stringWithString: @""];
    [numberStoreInArray removeAllObjects];
    [operatorStoreInArray removeAllObjects];
    txtInput.text = @"";
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
